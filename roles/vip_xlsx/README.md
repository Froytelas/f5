Role Name vip_xlsx
=========

Este role tiene como propocito automatizar la creación de granjas de tipo standard y performance_l4.

Requirements
------------

Se requiere un archivo xlsx con las variables para construir las granjas.

Role Variables
--------------

Adicional de las variables del xlsx se tienen definidos los profiles en /vip_xlsx/vars/main.yml

Dependencies
------------

Se requiere instalar los siguientes modulos de python en el host de control:
  - f5-sdk
  - bigsuds
  - netaddr
  - deepdiff
  - openpyxl

Example Playbook
----------------

- name: Create a balance service
  hosts: all
  roles:
    - vip_standard


Author Information
------------------

Juan Froylan Alanis Alanis

